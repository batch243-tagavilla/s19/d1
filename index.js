// console.log("TGIF!");

// What are conditional statements?

    // Conditional statements allows us to control the flow of our program.
    // It also allows us to run statement/intructions of a condition is met or run another separate instruction if otherwise.

// [Section] if, else if and else Statement

    let numA = -1;


    // if Statement - it will execute the statement if a specfied condtion is met/true.
        /* 
            Syntax:
                if (condition) {
                    statement;
                }
        */

        if(numA < 0) {
            console.log("Hello");
        }

        // The result of the expression added in the if's condtion must resilt to true, else the statement inside if() will not run.

        numA = 0;

        if(numA < 0) {
            console.log("Hello again");
        }

        let city = "New York";

        if(city === "New York") {
            console.log("Welcome to New York");
        }

    // else if clause 
        // executes a statement if previous conditions are false and if the specfied condition is true
        // The "else if" clause is optional and can be added to capture additonal conditions to change the flow of a program.

        let numH = 1;

        if (numH < 0) {
            console.log("Hello from numH");
        } else if (numH > 0) {
            console.log("Hi im H");
        }


        city = "Tokyo";

        if (city === "New York") {
            console.log("Welcome to New York");
        } else if (city === "Tokyo") {
            console.log("Welcome to Tokyo, Japan!")
        }

    // else Statement
        // executes a statement if all other conditions are false/not met.
        // is optional and can be added to capture any other result to change the flow of a program.

        numH = 2;

        if (numH < 0) {
            console.log("Hello I'm numH");
        } else if (numH > 2) {
            console.log("numH is greater than 2");
        } else if (numH > 3) {
            console.log("numH is greater than 3");
        } else {
            console.log("numH from else");
        }

    let message;

    function determineTyphoonIntensity(windSpeed) {
        if (windSpeed<0) {
            return "Invalid";
        } else if (windSpeed>=0 && windSpeed<=30) {
            return "Not a typhoon yet.";
        } else if (windSpeed<=60) {
            return "Tropical depression detected.";
        } else if (windSpeed>=61 && windSpeed<=88) {
            return "Tropical storm detected";
        } else if (windSpeed>=89 && windSpeed<=117) {
            return "Severe Tropical storm detected";
        } else {
            return "Typhoon detected!"
        }
    }

    message = determineTyphoonIntensity(118);
    console.log(message);


    // console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

    if (message === "Typhoon detected!") {
        console.warn(message);
    }

// [Section] Conditional (Ternary) Operator

    /* 
        1. condition
        2. expression to execute of the condition is truthy
        3. expression of the condition is falsy

        - can be used as an alternative to an "if else" statement
        - have implicit "return" statement meaning without return keyword, the resulting expression can be stored in a variable.
        - commonly used for single statement execution where the result consists of only one line of code.

        -Syntax-
        (expression) ? ifTrue : ifFalse;
    */

    // single statement execution
    let ternaryResult = (1<18) ? 1 : 2;
    console.log("Result of ternary operator: " + ternaryResult);

    // multiple state execution

    let yourName;

    function isLegalAge() {
        yourName = "John"
        return "You are of legal age";
    }

    function isUnderAge() {
        yourName = "Jane"
        return "You are under age limit";
    }

    // The parseInt() function converts input receive into a number data type
    // let age = parseInt(prompt("What is your age?"));
    let age = 18;
    console.log(age);

    let legalAge = (age >=18) ? isLegalAge() : isUnderAge();
    console.log(legalAge + ", " + yourName);

// [Section] Switch statement

    // let day = prompt("What day is it today? ").toLowerCase();

    let day = "saturday";

    switch (day) {
        case "monday":
            console.log("Monday");
            break;  
        case "tuesday":
            console.log("Tuesday");
            break;
        case "wednesday":
            console.log("Wednesday");
            break;  
        case "thursday":
            console.log("Thursday");
            break;
        case "friday":
            console.log("Friday");
            break;  
        case "saturday":
            console.log("Saturday");
            break;
        case "sunday":
            console.log("Sunday");
            break;
        default:
            console.log("Invalid day")
            break;
    }

// [Section] Try-catch-finally Statement

    // They are used to specify a response whenever an exception/erro is received.

    function showIntensityAlert (windSpeed) {
        try {
            if (false) {
                alerat(determineTyphoonIntensity(windSpeed));
            }
        } catch(error) {
            console.warn(error.message);
        } finally {
            alert("Intensity updates will show alert.");
        }
    }

    showIntensityAlert (110);